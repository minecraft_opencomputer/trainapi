local component = require("component")
local computer = require("computer")
local TrainControl = {
  _computer = computer,
  _component = component
}

function TrainControl:new()
  local o = {}
  setmetatable(o, self)
  if not self:hasAugmentDetector() and not self:hasAugmentController() and not self:hasRemoteController() then
    print("No Train Control Detected")
    os.exit()
  end
  if self:hasRemoteController() then
    self:getRemoteController()
  end
  if self:hasAugmentController() then
    self:getAugmentController()
  end
  if self:hasAugmentDetector() then
    self:getAugmentDetector()
  end
  self.__index = self
  return o
end

function TrainControl:hasRemoteController()
  return self._component.isAvailable("ir_remote_control")
end

function TrainControl:getRemoteController()
  if not self._remoteController then
    self._remoteController = self._component.getPrimary("ir_remote_control")
  end
  return self._remoteController
end

function TrainControl:hasAugmentController()
  return self._component.isAvailable("ir_augment_control")
end

function TrainControl:getAugmentController()
  if not self._augmentController then
    self._augmentController = self._component.getPrimary("ir_augment_control")
  end
  return self._augmentController
end

function TrainControl:hasAugmentDetector()
  return self._component.isAvailable("ir_augment_detector")
end

function TrainControl:getAugmentDetector()
  if not self._augementDetector then
    self._augementDetector = self._component.getPrimary("ir_augment_detector")
  end
  return self._augementDetector
end

function TrainControl:setThrottle(throttle)
  local controller = nil
  if self:hasRemoteController() then
    controller = self:getRemoteController()
  else
    if self:hasAugmentController() then
      controller = self:getAugmentController()
    end
  end
  if controller then
    return controller.setThrottle(throttle)
  else
    return false
  end
end

function TrainControl:setBrakes(brakes)
  local controller = nil
  if self:hasRemoteController() then
    controller = self:getRemoteController()
  else
    if self:hasAugmentController() then
      controller = self:getAugmentController()
    end
  end
  if controller then
    return controller.setBrake(brakes)
  else
    return false
  end
end

function TrainControl:getTrainSpeed()
  local speed = 0.0

  if self:hasRemoteController() then
    local posX, posY, posZ = self:getRemoteController().getPos()
    local time = self._computer.uptime()
    os.sleep(0.1)
    local newPosX, newPosY, newPosZ = self:getRemoteController().getPos()
    local newTime = self._computer.uptime()
    speed = math.sqrt(
      math.pow(newPosX - posX, 2) +
      math.pow(newPosY - posY, 2) +
      math.pow(newPosZ - posZ, 2)
    ) /
    (
      newTime - time
    ) *
    3.6
  end

  return speed
end

return TrainControl:new()
