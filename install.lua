local fileSystem = require("filesystem")
local component = require("component")

local disk
for address in component.list("filesystem") do
  local proxy = component.proxy(address)
  local proxyName = proxy.getLabel()
  if proxyName == "installTrainApi" then
    disk = proxy
  end
end

fileSystem.copy("/mnt/" .. disk.fsnode.name .. "/libs/trainControl.lua", "/lib/trainControl.lua")
