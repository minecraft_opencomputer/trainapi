local fileSystem = require("filesystem")
local component = require("component")
local drives = {}

-- "https://gitlab.com/minecraft_opencomputer/trainapi/-/raw/main/install.lua"
function getFile(drive, path, url)
  local internet = require("internet")
  local getFile = internet.request(url)
  local txtFile = ""
  for chunk in getFile do
    txtFile = txtFile .. chunk
  end
  local fileBuff = drive.open(path, "w")
  drive.write(fileBuff, txtFile)
  drive.close(fileBuff)
end

for address, type in component.list("filesystem", true) do
  table.insert(drives, component.proxy(address))
end

for index, proxy in pairs(drives) do
  local driveName = proxy.getLabel()
  if not driveName then
    driveName = ""
  end
  print(index .. " : " .. proxy.address .. " : " .. driveName)
end
print("Choisissez un disque :")
local driveChoice = tonumber(io.read())
local drive = drives[driveChoice]

if not drive then
  print "Erreur mauvais disque"
  os.exit()
end

drive.setLabel("installTrainApi")

if not drive.exists("libs") then
  drive.makeDirectory("libs")
end

getFile(drive, "install.lua", "https://gitlab.com/minecraft_opencomputer/trainapi/-/raw/main/install.lua")
getFile(drive, "libs/trainControl.lua", "https://gitlab.com/minecraft_opencomputer/trainapi/-/raw/main/libs/trainControl.lua")
