# TrainApi

Permet d'utiliser plus facilement l'api de ir_remote_control de immersive train.  
Cet api permet de calculer la vitesse du train, de set et de get les valeurs du train

## Contenu

trainControl.getRemoteController():table : Donne acces au proxy de ir_remote_controller

trainControl.getTrainSpeed():number : Renvoie la vitesse du train en Km/H

## TODO
- [X] Créer l'api
- [X] Ajouter les augments pour les trains
- [ ] Créer l'installeur
- [ ] Gérer le cas de plusieurs cartes
